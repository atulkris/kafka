const path = require('path');
const http = require('http');
const express = require('express');
const socketio = require('socket.io');
const formatMessage = require('./utils/messages');
const kafka = require('./utils/kafka')
const {
  userJoin,
  getCurrentUser,
  userLeave,
  getRoomUsers
} = require('./utils/users');
const app = express();
const server = http.createServer(app);
const io = socketio(server);

// Set static folder
app.use(express.static(path.join(__dirname, 'public')));

const producer = kafka.producer()

const consumer = kafka.consumer({
  groupId: 'con1'
})
const main = async () => {
await producer.connect()
await consumer.subscribe({
  topic: 'chat',
  fromBeginning: true
})

}
const botName = 'drakon';

// Run when client connects
io.on('connection', async socket => {
  socket.on('joinRoom', ({ username, room }) => {
    const user = userJoin(socket.id, username, room);

    socket.join(user.room);

    // Welcome current user
    socket.emit('message', formatMessage(botName, 'Welcome'));

    // Broadcast when a user connects
    socket.broadcast
      .to(user.room)
      .emit(
        'message',
        formatMessage(botName, `${user.username} has joined the chat`)
      );

    // Send users and room info
    io.to(user.room).emit('roomUsers', {
      room: user.room,
      users: getRoomUsers(user.room)
    });
  });

  // Listen for chatMessage
  socket.on('chatMessage',async msg => {
    const user = getCurrentUser(socket.id);
    if(user){
    const responses = await producer.send({
      topic: 'chat',
      messages: [{
        key: 'chatMessage',
        value: JSON.stringify({
          from:user.name,
          to:user.room,
          message:msg
        })
      }]
    })
  }

    // io.to(user.room).emit('message', formatMessage(user.username, msg));
  });
  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      // const messsage =JSON.parse(JSON)
      // io.to(user.room).emit('message', formatMessage(user.username, msg));
      // console.log('Received message', {
      //   topic,
      //   partition,
      //   message
      // });
      const recieved = JSON.parse(message.value.toString())
      io.to(recieved.to).emit('message', formatMessage(recieved.from, recieved.message));
    }
  })

  // Runs when client disconnects
  socket.on('disconnect', () => {
    const user = userLeave(socket.id);

    if (user) {
      io.to(user.room).emit(
        'message',
        formatMessage(botName, `${user.username} has left the chat`)
      );

      // Send users and room info
      io.to(user.room).emit('roomUsers', {
        room: user.room,
        users: getRoomUsers(user.room)
      });
    }
  });
});

const PORT = process.env.PORT || 3000;
main().catch(error => {
  console.error(error)
  process.exit(1)
})
server.listen(PORT, () => console.log(`Server running on port ${PORT}`));
