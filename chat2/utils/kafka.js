const { Kafka } = require('kafkajs')
const kafka = new Kafka({
  clientId: 'client-1',
  brokers: ['broker1:29001','broker2:29002','broker3:29003']
})

module.exports = kafka