from flask import Flask
from flask_socketio import SocketIO, emit
from json import dumps, loads
from kafka import KafkaProducer, KafkaConsumer
import threading
KAFKA_VERSION=(0, 10, 2)
KAFKA_SERVERS=['broker1:29001','broker2:29002','broker3:29003']
socketio = SocketIO(async_mode='gevent')

t1= threading.Thread()
def create_app(debug=True):
    app = Flask(__name__)
    def register_kafka_listener():
    # Poll kafka
        def poll():
            print("About to start polling for topic:",flush=True)
            consumer = KafkaConsumer(
                                bootstrap_servers=KAFKA_SERVERS, api_version=KAFKA_VERSION,
                                auto_offset_reset='earliest',
                                enable_auto_commit=True,
                                group_id='con-1',
                                max_poll_interval_ms=5000,
                                value_deserializer=lambda x: loads(x.decode('utf-8')))
            consumer.subscribe('chat')
            consumer.poll(10.0)
            print("Started Polling for topic:",flush=True)
            for message in consumer:
                print("New message:",flush=True)
                message=message.value
                emit('message', {'msg': message['from']+ ':' + message['msg']}, room=message['to'])
            # listener(message)
        print("About to register listener to topic:",flush=True)
        t1 = threading.Thread(target=poll)
        t1.start()
        print("started a background thread",flush=True)
    """Create an application."""
    
    app.debug = debug
    app.config['SECRET_KEY'] = 'gjr39dkjn344_!67#'

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    socketio.init_app(app)
    with app.app_context():
        register_kafka_listener()
    return app

