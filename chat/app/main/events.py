from flask import session
from flask_socketio import emit, join_room, leave_room
from .. import socketio
from json import dumps, loads
from kafka import KafkaProducer, KafkaConsumer
import threading
KAFKA_VERSION=(0, 10, 2)
KAFKA_SERVERS=['broker1:29001','broker2:29002','broker3:29003']
producer = KafkaProducer(bootstrap_servers=KAFKA_SERVERS, api_version=KAFKA_VERSION,
                         value_serializer=lambda x: 
                         dumps(x).encode('utf-8'))


@socketio.on('joined', namespace='/chat')
def joined(message):
    """Sent by clients when they enter a room.
    A status message is broadcast to all people in the room."""
    room = session.get('room')
    join_room(room)
    emit('status', {'msg': session.get('name') + ' has entered the room.'}, room=room)


@socketio.on('text', namespace='/chat')
def text(message):
    """Sent by a client when the user entered a new message.
    The message is sent to all people in the room."""
    room = session.get('room')
    # emit('message', {'msg': session.get('name') + ':' + message['msg']}, room=room)
    data = {'from' : session.get('name'), 'to': room, 'msg':message['msg']}
    producer.send('chat', value=data)
    producer.flush()


@socketio.on('left', namespace='/chat')
def left(message):
    """Sent by clients when they leave a room.
    A status message is broadcast to all people in the room."""
    room = session.get('room')
    leave_room(room)
    emit('status', {'msg': session.get('name') + ' has left the room.'}, room=room)


# def kafka_listener(message):
#     print(message,flush=True)
#     emit('message', {'msg': message['from']+ ':' + message['msg']}, room=message['to'])
# def register_kafka_listener():
#     # Poll kafka
#         def poll():
#             print("About to start polling for topic:",flush=True)
#             consumer = KafkaConsumer(
#                                 bootstrap_servers=KAFKA_SERVERS, api_version=KAFKA_VERSION,
#                                 auto_offset_reset='earliest',
#                                 enable_auto_commit=True,
#                                 group_id='con-1',
#                                 max_poll_interval_ms=5000,
#                                 value_deserializer=lambda x: loads(x.decode('utf-8')))
#             consumer.subscribe('chat')
#             consumer.poll(10.0)
#             print("Started Polling for topic:",flush=True)
#             for message in consumer:
#                 print("New message:",flush=True)
#                 message=message.value
#                 emit('message', {'msg': message['from']+ ':' + message['msg']}, room=message['to'])
#             # listener(message)
#         print("About to register listener to topic:",flush=True)
#         t1 = threading.Thread(target=poll)
#         t1.start()
#         print("started a background thread",flush=True)
# register_kafka_listener()
# consumer.subscribe(['chat'])
# print("Started Polling for topic:",flush=True)
# for message in consumer:
#     message=message.value
#     print("New message:",flush=True)
#     emit('message', {'msg': message['from']+ ':' + message['msg']}, room=message['to'])
